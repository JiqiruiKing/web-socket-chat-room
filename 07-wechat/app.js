/* 
  启动聊天的服务端程序
*/

var app = require("express")();
var server = require("http").Server(app);
var io = require("socket.io")(server);

//记录📝所有已经登陆过的用户
const users = [];

server.listen(3000, () => {
  console.log("服务器启动✅,端口号http://localhost:3000");
});

// express处理静态资源
// 把public目录设置为静态资源目录
app.use(require("express").static("public"));

app.get("/", (req, res) => {
  res.redirect("/index.html");
});

io.on("connection", (socket) => {
  console.log("新用户连接🔗上来了。。。");
  socket.on("login", (data) => {
    console.log("websocket-io中获取的数据：", data);
    // 判断，如果data在users中存在，就说明该用户已经登陆了，不允许登陆❎
    // 如果data在users中不存在，说明该用户没有登陆，允许用户登陆

    let user = users.find((item) => item.username === data.username);

    if (user) {
      // 表示用户存在，登陆失败、服务器需要给当前用户响应，告诉登陆失败
      socket.emit("loginError", { msg: "登陆失败❌" });
      // console.log("登陆失败！！！❌");
    } else {
      // 表示用户不存在，登陆成功
      users.push(data);
      socket.emit("loginSuccess", data);
      // console.log("登陆成功！！！✅");

      /* 
      广播📢消息：告诉所有的用户，有用户加入到了聊天室，广播消息
      socket.emit:告诉当前用户
      io.emit:广播📢事件

    */
      io.emit("addUser", data);

      // 告诉所有的用户，目前聊天室🈶️多少人
      io.emit("userlist", users);

      // 把登陆成功的用户名和头像存储起来
      const { username, avatar } = data;
      socket.username = username;
      socket.avatar = avatar;
    }
  });

  // 用户断开连接🔗的功能
  socket.on("disconnect", () => {
    // 把当前用户的信息从users中删除掉
    let idx = users.findIndex((item) => item.username === socket.username);
    // 删除断开🔗的那个人
    users.splice(idx, 1);

    //   1.告诉所有人，有人离开了聊天室
    io.emit("delUser", {
      username: socket.username,
      avatar: socket.avatar,
    });
    //   2.告诉所有人，userList发生更新
    io.emit("userList", users);
  });

  // 监听聊天的消息
  socket.on("sendMessage", (data) => {
    console.log("接受聊天的消息data", data);
    io.emit("reveiveMessage", data);
  });

  // 接受图片信息
  socket.on("sendImage", (data) => {
    // 广播给所有用户
    io.emit("reveiveImage", data);
  });
});
