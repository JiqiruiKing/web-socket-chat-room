/* 
  聊天室的主要功能
*/
/* 
  1. 连接socketio服务
*/
var socket = io("http://localhost:3000");

var username, avatar;

/* 
  2. 登录功能
*/
$("#login_avatar li").on("click", function () {
  $(this).addClass("now").siblings().removeClass("now");
});

// 点击按钮  登陆
$("#loginBtn").on("click", function () {
  // 获取用户名
  var username = $("#username").val().trim();
  if (!username) {
    alert("请输入用户名！");
    return;
  }
  // 获取选择的头像
  var avatar = $("#login_avatar li.now img").attr("src");
  console.log(username, avatar);

  // 需要告诉socket io 服务 登陆
  socket.emit("login", { username, avatar });
});

// 监听登陆失败的请求
socket.on("loginError", (data) => alert("登陆失败！（用户名已存在！）❌"));

// 监听登陆成功的请求
socket.on("loginSuccess", (data) => {
  // alert("登陆成功✅");
  // 需要隐藏登陆窗口，显示聊天窗口
  $(".login_box").fadeOut();
  $(".container").fadeIn();
  // 设置个人信息
  $(".avatar_url").attr("src", data.avatar);
  $(".username").text(data.username);
  username = data.username;
  avatar = data.avatar;
});

// 监听添加用户的消息
socket.on("addUser", (data) => {
  // 添加一条系统消息
  $(".box-bd").append(`
    <div class="system">
      <p class="message_system">
        <span class="content">欢迎👏---${data.username}👨‍🎓---加入群聊</span>
      </p>
    </div>
  
  `);
  scrollIntoView();
});

// 监听用户列表的消息
socket.on("userlist", (data) => {
  // 把userlist中的数据动态渲染到左侧菜单
  $("#userCount").text(data.length);
  $(".user-list ul").html("");
  data.forEach((item) => {
    // console.log("item的数据---》", item);
    $(".user-list ul").append(`
      <li class="user">
        <div class="avatar">
          <img src="${item.avatar}" alt="" />
        </div>
        <div class="name">${item.username}</div>
      </li>
    `);
  });
});

// 监听用户离开的消息
socket.on("delUser", (data) => {
  // 添加一条系统消息
  $(".box-bd").append(`
    <div class="system">
      <p class="message_system">
        <span class="content">注意---${data.username}👨‍🎓---离开群聊👋</span>
      </p>
    </div>
  
  `);
  scrollIntoView();
});

// 聊天功能
$("#btn-send").on("click", function () {
  // 获取到聊天的内容
  var content = $("#content").html().trim();
  $("#content").html("");
  if (!content) return alert("请输入内容！");
  // 发送给服务器
  socket.emit("sendMessage", {
    msg: content,
    username,
    avatar,
  });
});

// 监听聊天的消息
socket.on("reveiveMessage", (data) => {
  // console.log("接受到消息---", data);
  // 把接受到的消息提示道聊天窗口中
  if (data.username === username) {
    // 自己的消息
    $(".box-bd").append(`
      <div class="message-box">
        <div class="my message">
          <img class="avatar" src="${data.avatar}" alt="" />
          <div class="content">
            <div class="bubble">
              <div class="bubble_cont">${data.msg}</div>
            </div>
          </div>
        </div>
      </div>
    `);
  } else {
    $(".box-bd").append(`
      <div class="message-box">
        <div class="other message">
          <img class="avatar" src="${data.avatar}" alt="" />
          <div class="content">
            <div class="nickname">${data.username}</div>
            <div class="bubble">
              <div class="bubble_cont">${data.msg}</div>
            </div>
          </div>
        </div>
      </div>
    
    `);
  }
  scrollIntoView();
});

function scrollIntoView() {
  // 当前元素的底部滚动到可视区
  $(".box-bd").children(":last").get(0).scrollIntoView(false);
}

// 发送图片功能
$("#file").on("change", function () {
  var file = this.files[0];
  // 需要把这个文件发送到服务器，借助H5新增的fileReader
  var fr = new FileReader();
  fr.readAsDataURL(file);
  fr.onload = function () {
    socket.emit("sendImage", {
      username,
      avatar,
      img: fr.result,
    });
  };
});

// 监听图片聊天记录
socket.on("reveiveImage", (data) => {
  // console.log("接受到图片的聊天消息---", data);

  // 把接受到的消息提示道聊天窗口中
  if (data.username === username) {
    // 自己的消息
    $(".box-bd").append(`
      <div class="message-box">
        <div class="my message">
          <img class="avatar" src="${data.avatar}" alt="" />
          <div class="content">
            <div class="bubble">
              <div class="bubble_cont">
                <img src="${data.img}">
              </div>
            </div>
          </div>
        </div>
      </div>
    `);
  } else {
    $(".box-bd").append(`
      <div class="message-box">
        <div class="other message">
          <img class="avatar" src="${data.avatar}" alt="" />
          <div class="content">
            <div class="nickname">${data.username}</div>
            <div class="bubble">
              <div class="bubble_cont">
                <img src="${data.img}">
              </div>
            </div>
          </div>
        </div>
      </div>
    
    `);
  }
  // 等待图片加载完成 再滚动到底部
  $(".box-bd img:last").on("load", function () {
    scrollIntoView();
  });
});

// 初始化jquery-emoji出阿健
/* 
jquery-emoji：表情插件演示网址
https://www.jq22.com/yanshi6363

*/

$(".face").on("click", function () {
  $("#content").emoji({
    // 设置触发表情的按钮
    button: ".face",
    showTab: false,
    animation: "slide",
    position: "topRight",
    icons: [
      {
        name: "QQ表情",
        path: "../lib/jquery-emoji/img/qq/",
        maxNum: 91,
        excludeNums: [41, 45, 54],
        file: ".gif",
      },
    ],
  });
});
