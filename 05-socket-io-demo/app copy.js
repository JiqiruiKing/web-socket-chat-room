// 创建了http服务器

var app = require("http").createServe(handler);
var io = require("socket.io")(app);
var fs = require("fs");

app.listen(3000);

function handler(req, res) {
  fs.readFile(__dirname + "/index.html")((err, data) => {
    if (err) {
      res.writeHead(500);
      return res.end("加载失败。。。index.html");
    }
    res.writeHead(200);
    res.end(data);
  });
}

io.on("connection", (socket) => {
  socket.emit("news", { hello: "world" });
  socket.on("my other event", (data) => {
    console.log(data);
  });
});
