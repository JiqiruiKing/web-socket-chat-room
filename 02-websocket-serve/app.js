// 1.倒入nodejs-websocket包
const ws = require("nodejs-websocket");
const PORT = 3000;

// 2.创建一个serve
// 2.1如何处理用户的请求

// 每次只要有用户连接，函数就会被执行，会给当前连接的用户床架一个connect对象

const server = ws.createServer((connect) => {
  console.log("🈶️用户连接🔗上来了");
  // 每当结构到用户传递过来的数据，这个text事件就会被触发
  connect.on("text", (data) => {
    console.log("接收到用户的数据@@@", data);
    // 给用户响应的数据
    // 对用户发送过来的数据，把小些转换成大些，并且拼接一些内容
    connect.send(data.toUpperCase() + "@!!");
  });

  // 只要websocket连接断开，close事件就会触发
  connect.on("close", () => {
    console.log("连接断开了🔗断开");
  });

  // 注册一个error事件，处理用户的错误信息
  connect.on("error", () => {
    console.log("用户🔗连接异常");
  });
});

server.listen(PORT, () => {
  console.log("websocket服务启动成功了，监听了端口" + PORT);
});
